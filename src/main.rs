use std::process;
use structopt::StructOpt;

mod cli;

fn main() {
    let options = cli::Options::from_args();

    let filter = options.get_log_level_filter();
    pretty_env_logger::formatted_builder()
        .filter_module("create_roblox_project", filter)
        .init();

    log::trace!("initialized logger");

    if let Err(err) = options.run() {
        log::error!("{:?}", err);
        process::exit(1);
    }
}
