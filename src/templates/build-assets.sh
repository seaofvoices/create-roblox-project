#!/bin/sh

set -ex

mkdir -p build

rojo build -o build/{{PROJECT_NAME}}.{{FILE_EXTENSION}}
