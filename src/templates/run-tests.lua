local ReplicatedStorage = game:GetService('ReplicatedStorage')

local TestEZ = require(ReplicatedStorage:WaitForChild('TestEZ'))

local project = ReplicatedStorage:WaitForChild('{{PROJECT_NAME}}')

TestEZ.TestBootstrap:run({ project })
