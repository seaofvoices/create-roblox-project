#!/bin/sh

set -ex

TEST_PLACE=test-place.rbxlx
rojo build test-place.project.json -o $TEST_PLACE
run-in-roblox --place $TEST_PLACE --script {{TEST_RUNNER_PATH}}
