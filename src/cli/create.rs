use crate::cli::utils::{try_parse_feature, VALID_PROJECT_TYPES};

use std::env;
use std::path::PathBuf;
use std::str::FromStr;

use create_roblox_project::{Feature, Project};

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
enum ProjectType {
    Library,
}

impl FromStr for ProjectType {
    type Err = String;
    fn from_str(string: &str) -> Result<Self, <Self as FromStr>::Err> {
        Ok(match string {
            "library" => Self::Library,
            _ => {
                return Err(format!(
                    "invalid project type '{}', valid type{} {}",
                    string,
                    if VALID_PROJECT_TYPES.len() < 2 {
                        " is"
                    } else {
                        "s are"
                    },
                    VALID_PROJECT_TYPES.join(", "),
                ))
            }
        })
    }
}

impl Default for ProjectType {
    fn default() -> Self {
        Self::Library
    }
}

impl ToString for ProjectType {
    fn to_string(&self) -> String {
        match self {
            Self::Library => String::from("library"),
        }
    }
}

/// Creates a new Roblox project template.
///
/// The template can be customized by including or excluding different
/// features.
///
/// Example for excluding remodel: '--exclude remodel'
///
/// Example for including an MIT license: `--include mit-license`
#[derive(Debug, StructOpt)]
pub struct CreateCommand {
    /// Type of project you want to create
    #[structopt(long("type"), short("-t"), default_value)]
    project_type: ProjectType,

    /// The name of the project
    name: String,

    /// Where to create the new directory for the project
    #[structopt(long, short)]
    location: Option<PathBuf>,

    /// Features to be added (for each feature to include)
    #[structopt(long, short, parse(try_from_str = try_parse_feature))]
    include: Vec<Feature>,

    /// Features to excluded (for each feature to exclude)
    #[structopt(long, short, parse(try_from_str = try_parse_feature))]
    exclude: Vec<Feature>,
}

impl CreateCommand {
    pub fn run(&self) -> Result<(), String> {
        let mut project = match self.project_type {
            ProjectType::Library => Project::new_library(&self.name),
        };
        for feature in &self.include {
            project.with_feature(*feature);
        }
        for feature in &self.exclude {
            project.remove_feature(feature);
        }
        let work = project.create();
        let location = if let Some(location) = &self.location {
            location.clone()
        } else {
            env::current_dir()
                .map_err(|error| format!("unable to obtain current working directory: {}", error))?
        }
        .join(project.get_directory_name());

        if !location.exists() {
            work.execute(&location)
        } else {
            Err(format!(
                "unable to generate project: the directory {} already exists",
                location.display(),
            ))
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn suggested_types_are_valid() {
        for project_type_name in VALID_PROJECT_TYPES.iter() {
            let project_type = ProjectType::from_str(project_type_name);
            assert!(project_type.is_ok());
            assert_eq!(project_type_name, &project_type.unwrap().to_string())
        }
    }
}
