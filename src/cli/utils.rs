use create_roblox_project::Feature;

pub static VALID_PROJECT_TYPES: [&str; 1] = ["library"];

pub fn try_parse_feature(string: &str) -> Result<Feature, String> {
    Ok(match string.to_lowercase().as_str() {
        "rojo" => Feature::Rojo,
        "selene" => Feature::Selene,
        "testez" => Feature::TestEZ,
        "foreman" => Feature::Foreman,
        "scripts" => Feature::Scripts,
        "stylua" => Feature::Stylua,
        "remodel" => Feature::Remodel,
        "mit-license" => Feature::MitLicense,
        _ => return Err(format!("unknown feature '{}'", string)),
    })
}

pub fn feature_to_string(feature: &Feature) -> &'static str {
    match feature {
        Feature::Rojo => "rojo",
        Feature::Selene => "selene",
        Feature::TestEZ => "testez",
        Feature::Foreman => "foreman",
        Feature::Scripts => "scripts",
        Feature::Stylua => "stylua",
        Feature::Remodel => "remodel",
        Feature::MitLicense => "mit-license",
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use create_roblox_project::constants::ALL_FEATURES;

    #[test]
    fn stringified_features_can_be_parsed() {
        for feature in ALL_FEATURES.iter() {
            let stringified = feature_to_string(feature);
            let result = try_parse_feature(stringified).expect("unable to parse feature");
            assert_eq!(result, *feature);
        }
    }
}
