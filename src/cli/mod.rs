use log::LevelFilter;
use structopt::StructOpt;

mod create;
mod list;
mod utils;

#[derive(Debug, StructOpt)]
enum SubCommand {
    Create(create::CreateCommand),
    List(list::ListFeatures),
}

/// A tool to generate a template for starting a Roblox project.
#[derive(Debug, StructOpt)]
pub struct Options {
    /// Sets verbosity level. Can be specified multiple times.
    #[structopt(long("verbose"), short, global(true), parse(from_occurrences))]
    verbosity: u8,

    #[structopt(subcommand)]
    command: SubCommand,
}

impl Options {
    pub fn run(&self) -> Result<(), String> {
        match &self.command {
            SubCommand::Create(create) => create.run(),
            SubCommand::List(list_features) => list_features.run(),
        }
    }

    pub fn get_log_level_filter(&self) -> LevelFilter {
        match self.verbosity {
            0 => LevelFilter::Error,
            1 => LevelFilter::Warn,
            2 => LevelFilter::Info,
            3 => LevelFilter::Debug,
            _ => LevelFilter::Trace,
        }
    }
}
