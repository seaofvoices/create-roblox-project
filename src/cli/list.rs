use crate::cli::utils::{feature_to_string, VALID_PROJECT_TYPES};

use create_roblox_project::constants;

use structopt::StructOpt;

/// Lists the available project types and features
#[derive(Debug, StructOpt)]
pub struct ListFeatures {
    /// Lists the available features when creating a new project
    #[structopt(long, short)]
    features: bool,

    /// Lists the available project types that can be created
    #[structopt(long, short)]
    project_types: bool,
}

const LIST_PREFIX: &str = "  - ";

impl ListFeatures {
    pub fn run(&self) -> Result<(), String> {
        let at_least_one = self.features || self.project_types;
        let mut need_new_line = false;

        if self.project_types || !at_least_one {
            need_new_line = true;
            let mut names: Vec<_> = VALID_PROJECT_TYPES.iter().collect();
            names.sort();
            println!("available project types:");
            for name in names.iter() {
                println!("{}{}", LIST_PREFIX, name);
            }
        }

        if self.features || !at_least_one {
            if need_new_line {
                println!();
            }
            let mut names: Vec<_> = constants::ALL_FEATURES
                .iter()
                .map(|feature| (feature, feature_to_string(feature)))
                .collect();
            names.sort_by(|a, b| a.1.cmp(b.1));
            let longest_length = names.iter().map(|(_, name)| name.len()).max().unwrap_or(4);
            println!("available features:");
            names.iter().for_each(|(feature, name)| {
                let is_default = constants::DEFAULT_FEATURES
                    .iter()
                    .any(|default_feature| feature == &default_feature);
                println!(
                    "{}{:width$}{}",
                    LIST_PREFIX,
                    name,
                    if is_default {
                        " [included by default]"
                    } else {
                        ""
                    },
                    width = longest_length,
                );
            });
        }

        Ok(())
    }
}
