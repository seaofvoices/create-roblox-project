use git2::{Config, Repository};
use std::env;

fn get_environment_variable(variables: &[&str]) -> Option<String> {
    variables.iter().filter_map(|var| env::var(var).ok()).next()
}

pub fn discover_author() -> Option<(String, Option<String>)> {
    let cwd = env::current_dir().ok()?;
    let git_config = if let Ok(repo) = Repository::discover(&cwd) {
        repo.config().ok().or_else(|| Config::open_default().ok())
    } else {
        Config::open_default().ok()
    };
    let git_config = git_config.as_ref();
    let name_variables = [
        "GIT_AUTHOR_NAME",
        "GIT_COMMITTER_NAME",
        "USER",
        "USERNAME",
        "NAME",
    ];
    let name = get_environment_variable(&name_variables[0..2])
        .or_else(|| git_config.and_then(|g| g.get_string("user.name").ok()))
        .or_else(|| get_environment_variable(&name_variables[2..]))?;

    let email_variables = ["GIT_AUTHOR_EMAIL", "GIT_COMMITTER_EMAIL", "EMAIL"];
    let email = get_environment_variable(&email_variables[0..3])
        .or_else(|| git_config.and_then(|g| g.get_string("user.email").ok()))
        .or_else(|| get_environment_variable(&email_variables[3..]));

    let name = name.trim().to_string();
    let email = email.map(|s| s.trim().to_string());

    Some((name, email))
}
