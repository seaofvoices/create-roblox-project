use std::fmt::{Display, Formatter, Result};
use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Error {
    CannotWriteFile { path: PathBuf, reason: String },
    CannotCreateDirectory { path: PathBuf, reason: String },
    GitError { reason: String },
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            Self::CannotWriteFile { path, reason } => {
                write!(f, "unable to write file at {}: {}", path.display(), reason)
            }
            Self::CannotCreateDirectory { path, reason } => {
                write!(
                    f,
                    "unable to create directory at {}: {}",
                    path.display(),
                    reason
                )
            }
            Self::GitError { reason } => {
                write!(f, "an git error happened: {}", reason)
            }
        }
    }
}
