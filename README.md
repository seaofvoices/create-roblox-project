# create-roblox-project

This tool creates the initial structure of a new project by generating the various configuration files needed for the tools you use.

In a single command, setup a project configured to use:
* [Rojo](https://github.com/rojo-rbx/rojo)
* [Selene](https://github.com/Kampfkarren/selene)
* [TestEZ](https://roblox.github.io/testez)
* [Foreman](https://github.com/Roblox/foreman)
* [Remodel](https://github.com/rojo-rbx/remodel)
* [run-in-roblox](https://github.com/rojo-rbx/run-in-roblox)


## Installation

This project can be install using [cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)

```bash
cargo install create-roblox-project
```


## Usage

`create-roblox-project` has a help command that should make it easy to figure out how to the other commands works.

For generic help and a list of the available commands:
```bash
create-roblox-project help
```

For specific help about a command:
```bash
create-roblox-project help <enter command name>
# example with the 'create' command:
create-roblox-project help create
```

Each command also have a help parameter:
```bash
create-roblox-project <enter command name> --help
# example with the 'create' command:
create-roblox-project create --help
```

### `create`

This command is why this tool exists. First of all, choose the name of the project you want to create. Let's say we pick `CoolStuff`. Run the following command and see what happens:

```bash
create-roblox-project create CoolStuff
```

A new directory named `cool-stuff` has been created, let's open it with Visual Studio Code to look at what `create-roblox-project` has generated:
```bash
code CoolStuff # open VSCode from the command line
```

![Generated files](/imgs/cool-stuff-files.png)

You can see that a git repository was generated with a README, multiple configuration files, helper scripts and a `src` directory.

Currently, `create-roblox-project` only supports generating library templates. In the future, it can easily be extended to support new types of project, like a plugin or a place.

#### Including Features

You plan on open sourcing your project and want to add an MIT license? You can customize the create command to automatically add one:

```bash
create-roblox-project create CoolStuff --include mit-license
```

#### Excluding Features

You're not using [Remodel](https://github.com/rojo-rbx/remodel)? You can disable that feature simply by excluding the feature:

```bash
create-roblox-project create CoolStuff --exclude remodel
```

Not ready to use [Foreman](https://github.com/Roblox/foreman)? You can disable it too:

```bash
create-roblox-project create CoolStuff -e remodel -e foreman
```

To view the list of the available features and if they're included or not by default, you can use the `list` command:

```bash
create-roblox-project list --features
```

### `list`

A small utility command that prints the different features that can be included when generating a new project. Run the following command to get all the info you need:

```bash
create-roblox-project list
```


## Contributing

Check out the [contributing guide](CONTRIBUTING.md) to learn how you can help make this project better.

New to Rust? This project is fairly simple so it can be a good opportunity to get into it and write some code.


## License

`create-roblox-project` is available under the terms of the MIT license. See [LICENSE.txt](LICENSE.txt) for details.
