# Contributing

Thank you for your interest in contributing to this tool! There are different ways you can help this project become better.

## Code of Conduct

All contributors are expected to follow our [Code of Conduct](CODE_OF_CONDUCT.md).

## Issues

Issues are used for various reasons. They're used to communicate and organize the project. Here's a few general ideas of what you can do

* React to an issue: leaving a thumbs up or a heart emoji on an issue shows your interest for that issue. It can be **very** helpful when planning out the next features to add to the project.
* Replying to an issue: someone reported a bug and you found a way to reproduce it? Amazing! That's a really good thing to share to make it easier for maintainers to fix the problem. Giving feedback on a feature request or giving use case examples are also excellent examples of valuable help.
* Linking issues: you found duplicated issues? Write a comment that references the other issue, by using `#` followed by the issue number. For example, if an issue is duplicating the issue `7`, you can write a comment `Duplicate of #7`.

### Bugs

Before creating an issue to report a bug, search both opened and closed [issues](issues) to see if it does not exist already.

If you can, try to provide reproducing steps and the verbose mode output from the tool.

### Feature Requests

Have an idea about a new feature that could be added to the project? Create an issue and pitch it!

## Submitting Code

By contributing code to this project, you agree to license your contribution under the MIT license.

It's best to open an issue before contributing code, just to make sure that the new feature is ready to be integrated, or that the solution for a fix is appropriate for example. For trivial changes, like fixing a typo or re-wording the README, feel free to skip the issue and just submit a merge request.
