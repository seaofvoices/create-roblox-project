# `create-roblox-project` changelog

## Unreleased changes

- add [stylua](https://github.com/JohnnyMorganz/StyLua) config generation in default features (!1)
